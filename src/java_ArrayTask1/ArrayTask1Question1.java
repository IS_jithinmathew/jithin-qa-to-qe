
// Given the size and the elements of array A, print all the elements in reverse order.


package java_ArrayTask1;

import java.util.Scanner;

public class ArrayTask1Question1 {

	public static void main(String[] args) {
		
		
		System.out.println("Enter the size of the Array: ");
		
		Scanner inp=new Scanner(System.in);
		int n=inp.nextInt();
		int[] arr=new int[n];
		
		System.out.println("Enter the elements of the Array: ");
		for(int i=0;i<n;i++) {
			Scanner inp1=new Scanner(System.in);
			arr[i]=inp1.nextInt();
		}
		System.out.println("Reverse order of elements of the Array: ");
		
		for(int j=arr.length-1;j>=0;j--) {
			//System.out.println("Enter the ouput for loop ");
			System.out.println(arr[j]);
			
		}

	}

}
