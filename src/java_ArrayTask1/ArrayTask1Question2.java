// Micro purchased an array A having N integer values. After playing it for a while, 
//he got bored of it and decided to update value of its element. In one second he can increase 
//value of each array element by 1. He wants each array element's value to become greater than or 
//equal to K. Please help Micro to find out the minimum amount of time it will take, 
//for him to do so.

//----------------------------------------------------------------------

package java_ArrayTask1;

import java.util.Scanner;

public class ArrayTask1Question2 {

	public static void main(String[] args) {
		
		int T=0;
		int N=0;
		int K=0;
		
		
System.out.println("Enter the number of Test Cases: ");
		
		Scanner inp=new Scanner(System.in);
		T=inp.nextInt();
		
		System.out.println("Enter the N & K value ");	
		
		Scanner inp2=new Scanner(System.in);
		while (inp2.hasNext()) {
	        if (inp2.hasNextInt()) {
	        	N=inp2.nextInt();
	        	K=inp2.nextInt();
	        }
	            
	}
		System.out.println("Enter the N & K value "+N+" "+K);

}
}
