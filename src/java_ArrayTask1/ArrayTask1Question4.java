// Arijit is a brilliant boy. He likes memory games. He likes to participate alone 
//but this time he has to have a partner. So he chooses you.
//In this Game , your team will be shown N numbers for few minutes . 
//You will have to memorize these numbers.
//Now, the questioner will ask you Q queries, in each query He will give you a number , 
//and you have to tell him the total number of occurrences of that number in the array of
//numbers shown to your team . If the number is not present , then you will have to say �NOT PRESENT� (without quotes).

// ------------------------------------------------------------------------------------


package java_ArrayTask1;

import java.util.Scanner;

public class ArrayTask1Question4 {

	public static void main(String[] args) {
		
		int N=0;
		int Q=0;
		int count=0;
		int notPresent=0;
		
		
		System.out.println("Enter the number of numbers shown to your team: ");
		Scanner inp=new Scanner(System.in);
		N=inp.nextInt();
		int[] arr=new int[N];
			
		System.out.println("enter the numbers....");
		Scanner inp1=new Scanner(System.in);
		for(int i=0;i<N;i++) {
			
			
			arr[i]=inp1.nextInt();
			
		}
		
		System.out.println("the total number of integers....");
		
		Scanner inp2=new Scanner(System.in);
		Q=inp2.nextInt();
		int[] B=new int[Q];
		System.out.println("enter the numbers to see the number of occurences....");
		
		for(int i=0;i<Q;i++) {
			Scanner inp3=new Scanner(System.in);
			B[i]=inp3.nextInt();
			
		}
		for(int j=0;j<Q;j++) {
		for(int i=0;i<N;i++) {
			
			if(B[j]==arr[i]) {
				count++;
			}
			else {
				notPresent++;
				
			}
		}
		if(count>=1) {
		System.out.println(count);
		}
		count=0;
		if(notPresent==N) {
			System.out.println("NOT PRESENT ");
		}
		notPresent=0;
	}

}
}
