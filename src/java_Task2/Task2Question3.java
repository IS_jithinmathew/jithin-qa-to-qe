//You have been given 3 integers - l, r and k. 
//Find how many numbers between l and r (both inclusive) are divisible by k. 
//You do not need to print these numbers, you just have to find their count.


package java_Task2;

import java.util.Scanner;

public class Task2Question3 {

	public static void main(String[] args) {
	
		int len;
		int r;
		int k;
		int zz=0;
		int count=0;
		System.out.println("Enter the First Number: ");
		Scanner inp1=new Scanner(System.in);
		len=inp1.nextInt();
		System.out.println("Enter the Secound Number: ");
		Scanner inp2=new Scanner(System.in);
		r=inp2.nextInt();
		System.out.println("Enter the Number to divide: ");
		Scanner inp3=new Scanner(System.in);
		k=inp3.nextInt();
		
		
		int totalnum= (r-len)+1;
		
		for(int i=len;i<=r;i++) {
			
			zz=i%k;
			
			if(zz==0) {
				count++;
			}
			
			
		}
		System.out.println("Total "+count+" numbers can be divisible by "+k);

	}

}
