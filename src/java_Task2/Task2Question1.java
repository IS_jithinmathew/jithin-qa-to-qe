

//You have been given a positive integer N. You need to find and print the Factorial of this 
//number. The Factorial of a positive integer N refers to the product of all 
//number in the range from 1 to N.



package java_Task2;

import java.util.Scanner;

public class Task2Question1 {

	public static void main(String[] args) {
		
		int N=0;
		System.out.println("Enter a Number: ");
		Scanner numinp=new Scanner(System.in);
		N=numinp.nextInt();
		
		if(N!=0) {
		int result=1;
		for(int i=1;i<=N;i++) {
			
			result = result*i;
			
		}
		
		System.out.println("Factorial is = "+result);
		}else
	
			System.out.println("You have entered 0 ... ");
	}

	}


